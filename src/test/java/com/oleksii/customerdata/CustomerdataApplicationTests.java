package com.oleksii.customerdata;

import com.oleksii.customerdata.entities.Customer;
import com.oleksii.customerdata.repos.CustomerRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

@SpringBootTest
class CustomerdataApplicationTests {

    @Autowired
    CustomerRepository repo;

    @Test
    void testCreateCustomer() {
        Customer customer = new Customer();
        customer.setName("John");
        customer.setEmail("john@example.com");

        repo.save(customer);
    }

    @Test
    void testReadCustomer() {
        Customer customer = repo.findById(1L).get();

        System.out.println(customer);
    }

    @Test
    void testUpdateCustomer() {
        Customer customer = repo.findById(1L).get();
        customer.setEmail("other@example.com");

        repo.save(customer);
    }

    @Test
    void testDeleteCustomer() {
        Customer customer = repo.findById(1L).get();

        repo.delete(customer);
    }

}
