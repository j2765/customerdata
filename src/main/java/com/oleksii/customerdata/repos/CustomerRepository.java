package com.oleksii.customerdata.repos;

import com.oleksii.customerdata.entities.Customer;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer, Long> {}
